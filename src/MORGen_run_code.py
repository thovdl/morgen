# Toolbox
from MORGen_functions import pygplates, get_MOR_list, select_curves, create_stepped_lines, make_plate_id_list, get_plate_features, get_ordered_features, make_boundary_topology, make_isochron, create_isochrons_for_boundary_topologies, check_for_subduction_individual_isochrons, create_age_grid
from MORGen_set_parameters import GPML_List, rotation_file, model, from_time, to_time, timestep, tessellate, tessellate_radians, on_intermediate, grid_times, convert_curves, create_polygons, create_isochrons, isochrons_from_lines, isochrons_from_boundary_topologies, check_subduction, make_age_grids, include_subduction, exclude_subduction, make_bathymetry, res, region
import numpy as np

# tool 1
# Convert curves to stepped lines at times
# Input can be lines, line topologies or boundary topologies (preferably only one type at a time)
# If input is topologies, the fringes are cut before making the steps
# Output is one file with stepped lines and one file with other elements
# Euler pole change times or fixed start,end,timestep
# Write stepped lines to file

def curve_convertor(feature_collection, from_time, to_time, timestep, rotation_model, on_intermediate, tessellate, tessellate_radians, model):

    plate_ids_list = get_MOR_list(feature_collection)
    MOR_out = pygplates.FeatureCollection()
    times_list = []
    full_curves_list = []

    # remove duplicate plate id pairs (x,y) == (y,x)
    plate_ids_list = set([tuple(sorted(plate_ids)) for plate_ids in plate_ids_list])
    
    for plate_ids in plate_ids_list:
        curves = select_curves(feature_collection, plate_ids)
        for curve in curves:
            full_curves_list.append(curve)

            times = list(range(from_time, to_time - (timestep * 2), -timestep))
            times_list.append(times)

            for i in range(1, len(times)):
                begin_time = times[i-1]
                end_time = times[i]
                MOR_out = create_stepped_lines(plate_ids, rotation_model, begin_time, end_time, timestep, curve, MOR_out, on_intermediate, tessellate, tessellate_radians)
            MOR_out.write(f'../data/temp/MOR_from_curve_{model}.gpml')
    
    return MOR_out, full_curves_list

# tool 2
# Combine lines (from tool 1) to self closing polygons at times
# Ordering works on cutting lines
# Write self closing polygons to a file

def polygon_creator(feature_collection, full_curves_list, MOR_out, from_time, to_time, timestep, model):

    # remove the original curves from the feature collection
    # add the newly drawn MORs
    # this requires elements to have a plate id and conjugate plate id of the other side
    # might be necessary to have multiple copies of the same feature
    # for instance Andean subduction zone: 201 and 901, 201 and 902, 201 and 908, ...
    # also MOR curves can be cut short by converting to stepped lines
    # making the polygons depends on cutting lines, that might fail if MORs are cut

    replaced_curves = pygplates.FeatureCollection()

    for feature in feature_collection:
        if feature not in full_curves_list:
            if feature.get_feature_type().get_name() != 'TopologicalClosedPlateBoundary':
                replaced_curves.add(feature)
    replaced_curves.add(MOR_out)
    replaced_curves.write(f'../data/temp/curves_replaced_{model}.gpml')
    feature_collection = replaced_curves

    # create self closing polygons
    times_list = list(list(range(from_time, to_time-timestep, -timestep)))

    topologies = []

    for time in times_list:
        print(f'starting time {time}')
        plate_id_list = make_plate_id_list(feature_collection)
        if 0 in plate_id_list:
            plate_id_list.remove(0)

        for plate_id in plate_id_list:
            plate_features = get_plate_features(feature_collection, plate_id)
            reconstructed_feature_geometries = []
            pygplates.reconstruct(plate_features, rotation_model, reconstructed_feature_geometries, time)

            if len(reconstructed_feature_geometries) > 2:
                ordered_features = get_ordered_features(reconstructed_feature_geometries)

                topological_polygon_feature = make_boundary_topology(ordered_features, plate_id, time, timestep)
                if topological_polygon_feature is not None:
                    topologies.append(topological_polygon_feature)

    topologies = pygplates.FeatureCollection(topologies)
    topologies.add(replaced_curves)     
    topologies.write(f'../data/temp/{model}_topologies.gpml') 

    return feature_collection, topologies

# tool 3
# Use self closing polygons (from tool 2) to create isochrons at times
# Check points in isochrons for subduction at times
# Write isochrons to one file and subducted points to one file

def isochron_creator_from_lines(from_time, to_time, timestep, model, feature_collection, rotation_model):
    isochron_feature_collection = pygplates.FeatureCollection()
    times_list = list(list(range(from_time, to_time-timestep, -timestep)))

    feature_collection = pygplates.FeatureCollection(f'../data/temp/MOR_from_curve_{model}.gpml')
    plate_ids_list = get_MOR_list(feature_collection)
    
    for plate_ids in plate_ids_list:
        print(f'starting {plate_ids} at times {times_list}')
        line_features = select_curves(feature_collection, plate_ids)
        isochron_feature_collection = make_isochron(times_list, plate_ids, rotation_model, timestep, line_features, isochron_feature_collection)
        isochron_feature_collection.write(f'../data/temp/isochrons_lines_{model}.gpml')

    return isochron_feature_collection

def isochron_creator_from_boundary_topologies(model, topologies, rotation_model, from_time, to_time, timestep, tessellate, tessellate_radians, on_intermediate):
    isochron_feature_collection = pygplates.FeatureCollection()
    isochron_feature_collection = create_isochrons_for_boundary_topologies(topologies, rotation_model, from_time, to_time, timestep, isochron_feature_collection, tessellate, tessellate_radians, model, on_intermediate)
    isochron_feature_collection.write(f'../data/temp/isochrons_boundary_topologies_{model}.gpml')

    return isochron_feature_collection

# tool 4
# check for subduction

def subduction_check(from_time, to_time, timestep, model, isochron_feature_collection, feature_collection, rotation_model):

    times = list(range(from_time, to_time-timestep, -timestep))

    isochron_features_not_subducted = pygplates.FeatureCollection()
    subducted_isochrons = pygplates.FeatureCollection()
    other_isochrons = pygplates.FeatureCollection()

    isochron_features_not_subducted, subducted_isochrons, other_isochrons = check_for_subduction_individual_isochrons(isochron_feature_collection, feature_collection, rotation_model, times, timestep, isochron_features_not_subducted, subducted_isochrons, other_isochrons)
    print('saving isochrons after subduction')
    isochron_features_not_subducted.write(f'../data/temp/isochrons_{model}_after_subduction.gpml')
    subducted_isochrons.write(f'../data/temp/isochrons_{model}_subducted.gpml')
    other_isochrons.write(f'../data/temp/isochrons_{model}_other.gpml')

    return isochron_features_not_subducted, subducted_isochrons, other_isochrons




feature_collection = pygplates.FeatureCollection()

for GPML_file in GPML_List:
    features = pygplates.FeatureCollection(GPML_file)
    feature_collection.add(features)

rotation_model = pygplates.RotationModel(rotation_file)

print(f'starting {model}')

if convert_curves:
    print('start converting curves')
    MOR_out, full_curves_list = curve_convertor(feature_collection, from_time, to_time, timestep, rotation_model, on_intermediate, tessellate, tessellate_radians, model)

if create_polygons:
    print('start making topological plate boundaries')
    feature_collection, topologies = polygon_creator(feature_collection, full_curves_list, MOR_out, from_time, to_time, timestep, model)

if create_isochrons:
    print(f'start creating isochrons')
    if isochrons_from_lines:
        isochron_feature_collection = isochron_creator_from_lines(from_time, to_time, timestep, model, feature_collection, rotation_model)
    elif isochrons_from_boundary_topologies:
        if not create_polygons:
            topologies = pygplates.FeatureCollection(f'../data/temp/{model}_topologies.gpml')
        isochron_feature_collection = isochron_creator_from_boundary_topologies(model, topologies, rotation_model, from_time, to_time, timestep, tessellate, tessellate_radians, on_intermediate)

if check_subduction:
    print(f'start checking subduction')
    if not create_isochrons:
        isochron_feature_collection = pygplates.FeatureCollection(f'../data/temp/isochrons_boundary_topologies_{model}.gpml')
    isochron_features_not_subducted, subducted_isochrons, other_isochrons = subduction_check(from_time, to_time, timestep, model, isochron_feature_collection, feature_collection, rotation_model)

# tool 5
# convert isochrons to age grids
if make_age_grids:
    print(f'start making age grid')
    # create age grids at one or more times

    if check_subduction:            
        if include_subduction:
            age_in = 'isochron_age'
            isochron_feature_collection = isochron_features_not_subducted
        elif exclude_subduction:
            age_in = 'valid_time'
            isochron_feature_collection = isochron_feature_collection

    else:
        if include_subduction:
            age_in = 'isochron_age'
            isochron_feature_collection = pygplates.FeatureCollection(f'../data/temp/isochrons_{model}_after_subduction.gpml')
        elif exclude_subduction:
            age_in = 'valid_time'
            if isochrons_from_boundary_topologies:
                isochron_feature_collection = pygplates.FeatureCollection(f'../data/temp/isochrons_boundary_topologies_{model}.gpml')
            elif isochrons_from_lines:
                isochron_feature_collection = pygplates.FeatureCollection(f'../data/temp/isochrons_lines_{model}.gpml')

    for time in grid_times:
        print(f'start time {time}')
        create_age_grid(isochron_feature_collection, rotation_model, time, age_in, res, model, region)

if make_bathymetry:
    print('start making depth grid')
    from netCDF4 import Dataset
    for time in grid_times:
        netcdf = Dataset(f'../data/temp/age-grid_{model}_{time}Ma.nc', 'r')
        age = np.asarray(netcdf['z'])
        lats = np.asarray(netcdf['lat'])
        lons = np.asarray(netcdf['lon'])
        netcdf.close()

        OceanFloor = np.full(age.shape, np.NaN)
        # Determine depth using age-depth relationship
        OceanFloor = np.where(age - time > 0, -2620 - 330 * np.sqrt(age - time), np.NaN)
        OceanFloor = np.where(age - time > 90, -5750, OceanFloor)

        ncfile = Dataset(f'../data/temp/depth_grid_{model}_{time}Ma.nc', 'w')

        lat_dim = ncfile.createDimension('lat', len(lats))
        lon_dim = ncfile.createDimension('lon', len(lons))
        depth_dim = ncfile.createDimension('depth', None)

        lat = ncfile.createVariable('lat', np.float32, ('lat',))
        lat.units = 'degrees_north'
        lat.long_name = 'latitude'
        lon = ncfile.createVariable('lon', np.float32, ('lon',))
        lon.units = 'degrees_east'
        lon.long_name = 'longitude'
        depth = ncfile.createVariable('depth', np.float64, ('lat','lon'))
        depth.units = 'm'
        depth.long_name = 'bathymetry'

        lat[:] = lats
        lon[:] = lons
        depth[:] = OceanFloor

        ncfile.title = f'depth_grid_{model}_{time}Ma'

        ncfile.close()
