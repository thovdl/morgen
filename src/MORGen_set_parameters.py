import math
import glob

model = 'southern_oceans'

# TODO: variables in camel case

if model == 'future':
    folder = "../data/raw/plateReconstructions/future/"
    GPML_List = [folder + 'linear_features.gpml'] # location of input gpml file
    rotation_file = folder + "future.rot" # location of input rot file
    from_time, to_time = 200, 0 # start and end time for reconstruction
    grid_times = [0] # times for which a grid is created
    timestep = 5 # value is used for the steps at which isochrons are calculated
    tessellate = True # choice to divide curve in segments with set length (tesselate_radians)
    tessellate_radians = math.radians(4) # 1 results in segments of approximately 70 km
    on_intermediate = 'transforms' # choice for either transforms or ridges on intermediate points
    region = "40/140/-60/26" # for grid, xmin/xmax/ymin/ymax use "d" for the whole globe
    res = 0.5 # resolution in degrees for grids

    convert_curves = True # choice to convert curve to stepped geometry
    create_polygons = False # choice to combine linear features to polygons
    create_isochrons = True # choice to create isochrons from MOR features
    check_subduction = False # choice to check whether isochrons have been subducted
    make_age_grids = True # create an age grid from isochrons
    make_bathymetry = True # convert the age grid to depths

    # choice of which type of features to use as source for isochrons
    isochrons_from_lines = True # use non-modified lines
    isochrons_from_boundary_topologies = False # similar to isochrons_from_line_topologies but using polygons

    # choice of whether to include subducted parts of isochrons in creating the age grid
    include_subduction = False # use only isochrons that remain on the surface of the planet
    exclude_subduction = True # use all isochrons

if model == 'southern_oceans':
    folder = "../data/raw/plateReconstructions/southern_oceans/"
    GPML_List = glob.glob(folder + '*.gpml')
    rotation_file = folder + "southern_oceans.rot"
    from_time, to_time = 120, 0
    grid_times = [0]
    timestep = 10 # value is used for the steps at which isochrons are calculated
    tessellate = True
    tessellate_radians = math.radians(1) # 1 results in segments of approximately 70 km
    on_intermediate = 'transforms'
    region = "-60/22/-67/10" # for grid, xmin/xmax/ymin/ymax use "d" for the whole globe
    res = 0.5 # resolution in degrees for grids

    convert_curves = True
    create_polygons = True
    create_isochrons = True
    #sub options for create_isochrons    
    isochrons_from_lines = False
    isochrons_from_boundary_topologies = True
    
    check_subduction = True
    make_age_grids = True
    make_bathymetry = False

    include_subduction = True
    exclude_subduction = False

if model == 'mediterranean':
    folder = "../data/raw/plateReconstructions/mediterranean/"
    GPML_List = [folder + "mediterranean.gpml"]
    rotation_file = folder + "mediterranean.rot"
    from_time, to_time = 240, 165
    grid_times = [170]
    timestep = 5 # value is used for the steps at which isochrons are calculated
    tessellate = True
    tessellate_radians = math.radians(4) # 1 results in segments of approximately 70 km
    on_intermediate = 'transforms'
    region = "0/48/2/40" # for grid, xmin/xmax/ymin/ymax use "d" for the whole globe
    res = 0.5 # resolution in degrees for grids

    convert_curves = True   
    create_polygons = False
    create_isochrons = True
    # sub options for create_isochrons
    isochrons_from_lines = True
    isochrons_from_boundary_topologies = False
    
    check_subduction = False
    make_age_grids = False
    make_bathymetry = False

    include_subduction = False
    exclude_subduction = False