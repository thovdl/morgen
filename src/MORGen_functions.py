"""
Convert a curve to a system of MOR and transform faults
It requires a line and two plates that move relative to each other
"""

print('importing')
import sys
if sys.platform.startswith('win'):
    sys.path.insert(1, '../bin/external/pygplates_rev28_python37_win64')
if sys.platform.startswith('lin'):
    sys.path.insert(1, '/usr/lib/pygplates/revision28')
if sys.platform.startswith('Mac'):
    sys.path.append('../bin/external/pygplates_rev28_python37_MacOS64')
import pygplates
import numpy as np
import math
import os

if not os.path.isdir('../data/temp'):
    os.mkdir('../data/temp')

def get_MOR_list(feature_collection):
    # this function creates a list of pairs of plate ids for mid ocean ridges
    plate_ids_list = []

    for feature in feature_collection:
        if feature.get_feature_type().get_name() == 'MidOceanRidge':
            left_plate = feature.get_left_plate()
            right_plate = feature.get_right_plate()
            plate_ids_list.append((left_plate, right_plate))

    # take out the doubles
    plate_ids_list = list(set(plate_ids_list))
    
    # remove MOR between the same plate
    for plate_pair in plate_ids_list:
        if plate_pair[0] == plate_pair[1]:
            plate_ids_list.remove(plate_pair)

    plate_ids_list = [list(pair) for pair in plate_ids_list]

    return plate_ids_list


def select_curves(feature_collection, plate_ids):
    # this function selects the required curve (only HalfStageRotation) from the feature_collection    
    feature_selection = pygplates.FeatureCollection()
    
    for feature in feature_collection:
        if feature.get_feature_type().get_name() == 'MidOceanRidge':
            if feature.get_left_plate() in plate_ids and feature.get_right_plate() in plate_ids:
                feature_selection.add(feature)
    
    return feature_selection

def get_curve_geometry(feature_selection, rotation_model, reconstruction_time, plate_ids):
    # this function provides the geometry of the curve at a reconstruction time
    reconstructed_geometries = []
    
    pygplates.reconstruct(feature_selection, rotation_model, reconstructed_geometries, reconstruction_time)

    reconstructed_curves = []
    for geometry in reconstructed_geometries:
        reconstructed_curves.append(geometry.get_reconstructed_geometry())
    
    return reconstructed_curves

def get_stage_pole(plate_ids, reconstruction_time, rotation_model, timestep):
    # this function provides the relative rotation pole at a time, with one timestep forward
    equivalent_total_rotation1 = rotation_model.get_rotation(to_time = reconstruction_time, 
                                                            moving_plate_id = plate_ids[0],
                                                            from_time = reconstruction_time - timestep,
                                                            anchor_plate_id = plate_ids[1])

    pole1 = equivalent_total_rotation1.get_euler_pole_and_angle()[0].to_xyz()

    return pole1

def get_angles(curve):
    # this function provides a check for approximately right angles in curves
    # it is of little use, thought it could be used for checking loops,
    # but also in loops all angles are ~90deg
    angles = []
    
    for i in range(len(curve) - 2):
        vectorA = pygplates.Vector3D(curve[i].to_xyz()).to_normalised() - pygplates.Vector3D(curve[i+1].to_xyz()).to_normalised()      
        vectorB = pygplates.Vector3D(curve[i+2].to_xyz()).to_normalised() - pygplates.Vector3D(curve[i+1].to_xyz()).to_normalised()
        
        dot_product = pygplates.Vector3D.dot(vectorA, vectorB)
    
        angle = math.acos(dot_product)
        angles.append(round(math.degrees(angle)))
                
    return angles

def tessellate_curve(curve, plate_ids, tessellate_radians, tessellated_curves, reconstruction_time, end_time, rotation_model):
    # this function increases the number of points on the curve to a maximum distance between points
    # it is used to make drawing of the curve easier, while providing an end result with realistic distances between transform faults
    curve = pygplates.PolylineOnSphere(curve)
    
    tessellated_curve = curve.to_tessellated(tessellate_radians)

    tessellated_curve_feature = pygplates.Feature.create_tectonic_section(
        pygplates.FeatureType.gpml_mid_ocean_ridge,
        tessellated_curve,
        valid_time=(reconstruction_time, end_time),
        left_plate=plate_ids[0],
        right_plate=plate_ids[1],
        reconstruction_method='HalfStageRotationVersion3',
        reverse_reconstruct=(rotation_model, reconstruction_time)
        )

    tessellated_curves.add(tessellated_curve_feature)

    return tessellated_curve

def get_intermediate_points(curve):
    # this function provides the coordinates of points between the points defining the line
    # these points are used to draw the great circles (spreading ridge segments)
    intermediate_points = []
    
    for i in range(len(curve)-1):
        x = (curve[i][0] + curve[i+1][0])
        y = (curve[i][1] + curve[i+1][1])
        z = (curve[i][2] + curve[i+1][2])

        intermediate_point = pygplates.PointOnSphere(x, y, z, normalise=True)

        intermediate_points.append(intermediate_point)
    
    return intermediate_points
    
def create_greatcircles(point1, point2, time, plate_ids, MORs, rotation_model):
    # this function provides line segments on the great circles between two provided points 

    # create a line geometry from the two points    
    geometry_at_time_of_appearance = pygplates.PolylineOnSphere([point1, point2])
    
    # create a feature with this line geometry
    mid_ocean_ridge_feature = pygplates.Feature.create_tectonic_section(
        pygplates.FeatureType.gpml_mid_ocean_ridge,
        geometry_at_time_of_appearance,
        valid_time=(time, 0),
        left_plate=plate_ids[0],
        right_plate=plate_ids[1],
        reconstruction_method='HalfStageRotationVersion3',
        reverse_reconstruct=(rotation_model, time))
        
    MORs.add(mid_ocean_ridge_feature)
    
    return MORs

def create_transforms(start_point, end_point):
    # TODO: check other option to create small circles https://discourse.gplates.org/t/how-to-create-a-small-circle-features-in-pygplates/

    # this function provides vectors at 90 deg to the great circle segment formed by the two provided points
    # the vectors are used to construct the transform fault elements of the MOR

    great_circle = pygplates.GreatCircleArc(start_point, end_point)
    # divide by factor to prevent problems with spherical projection of long straight vector instead of small circle
    # gplates only does multiplication therefore factor <1

    #TODO: this could be made into more segments. Previous values were too small to get all intersections, but the current value might be so large the error gets too large (circle vs. straight line)
    great_circle_normal = great_circle.get_great_circle_normal() * 0.1
    
    return great_circle_normal

def points_to_transform(point, vector, reconstruction_time, end_time, rotation_model, transforms, plate_ids, on_intermediate):
    # this function applies the vectors from the small circles 
    # to the points and creates transform fault elements of the mid ocean ridges
    if on_intermediate == 'transforms':
        point = pygplates.Vector3D(point.to_xyz())
    if on_intermediate == 'ridges':
        point = pygplates.Vector3D(point)
    vector1 = point + vector
    vector2 = point - vector
    vector1 = vector1.to_normalised().to_xyz()
    vector2 = vector2.to_normalised().to_xyz()
    point1 = pygplates.PointOnSphere(vector1)
    point2 = pygplates.PointOnSphere(vector2)
    
    geometry_at_time_of_appearance = pygplates.PolylineOnSphere([point1, point2])
    
    transform_feature = pygplates.Feature.create_tectonic_section(
        pygplates.FeatureType.gpml_transform,
        geometry_at_time_of_appearance,
        valid_time=(reconstruction_time, end_time),
        left_plate=plate_ids[0],
        right_plate=plate_ids[1],
        reconstruction_method='HalfStageRotationVersion3',
        reverse_reconstruct=(rotation_model, reconstruction_time))
    
    transforms.add(transform_feature)
        
    return transforms

def get_intersections(MORs1, MORs2, transforms, rotation_model, reconstruction_time, on_intermediate, curve):
    # this function provides the intersections between the great circles segments and small circle segments
    # there are two sets of MOR geometries, one from the Euler pole and one from its antipodal pole
    intersections = []
    MOR_geoms1 = []
    MOR_geoms2 = []
    transform_geoms = []

    # get the geometries
    for MOR in MORs1:
        for geometry in MOR.get_geometries():
            MOR_geoms1.append(geometry)

    for MOR in MORs2:
        for geometry in MOR.get_geometries():
            MOR_geoms2.append(geometry)

    for transform in transforms:
        for geometry in transform.get_geometries():
            transform_geoms.append(geometry)

    # either spreading ridges or transforms can be drawn on intermediate points
    # the other feature type has one more element
    # therefore there are two options to get the intersections
    if on_intermediate == 'ridges':
        # add the first point of the curve as this will not give an intersection
        intersections.append(transform_geoms[0][0])

        for i in range(len(MOR_geoms1)):
            MOR1 = MOR_geoms1[i]
            MOR2 = MOR_geoms2[i]
            transform1 = transform_geoms[i]
            transform2 = transform_geoms[i+1]

            distance = pygplates.PolylineOnSphere.distance(MOR1, transform1, return_closest_positions=True)
            if distance[0] == 0.:
                geometry = pygplates.PointOnSphere(distance[1])
                intersections.append(geometry)
        
            distance = pygplates.PolylineOnSphere.distance(MOR2, transform2, return_closest_positions=True)
            if distance[0] == 0.:
                geometry = pygplates.PointOnSphere(distance[1])
                intersections.append(geometry)

        # add the last point of the curve as this will not give an intersection
        intersections.append(transform_geoms[-1][0])

    if on_intermediate == 'transforms':

        intersections.append(MOR_geoms1[0][0])

        for i in range(len(MOR_geoms1)-1):
            MOR1 = MOR_geoms1[i]
            MOR2 = MOR_geoms2[i+1]
            transform1 = transform_geoms[i]
            transform2 = transform_geoms[i]

            distance = pygplates.PolylineOnSphere.distance(MOR1, transform1, return_closest_positions=True)
            if distance[0] == 0.:
                geometry = pygplates.PointOnSphere(distance[1])
                intersections.append(geometry)
        
            distance = pygplates.PolylineOnSphere.distance(MOR2, transform2, return_closest_positions=True)
            if distance[0] == 0.:
                geometry = pygplates.PointOnSphere(distance[1])
                intersections.append(geometry)
        
        intersections.append(MOR_geoms1[-1][0])

    # sometimes this returns a loop
    # check for an almost closed line and remove the wrong point
    # some loops are not found this way as the loop does not go to one of the end points,
    # these might be due to wrong drawing of curve

    if len(intersections) > 2 and pygplates.GeometryOnSphere.distance(intersections[0], intersections[-1]) < 0.1:
        dist1 = pygplates.GeometryOnSphere.distance(intersections[0], intersections[1])
        dist2 = pygplates.GeometryOnSphere.distance(intersections[0], intersections[-2])
        if dist1 > dist2:
            del intersections[0]
        if dist1 < dist2:
            del intersections[-1]

    return intersections

def intersections_to_MOR(intersections, reconstruction_time, end_time, timestep, plate_ids, rotation_model, MOR_out):
    # this function converts the list of intersection points to a MOR feature  

    # increase the number of points on the geometry to improve the interpolation later on
    # create a pygplates geometry so it can be converted to segments
    geometry = pygplates.PolylineOnSphere(intersections)
    segments = list(geometry.get_segments())

    # tessellate only the spreading centres, not the transforms
    tessellated_geometry = []
    for i, segment in enumerate(segments):
        if i % 2 == 0:
            for point in segment.to_tessellated(math.radians(0.1)):
                tessellated_geometry.append(point)

    tessellated_geometry = pygplates.PolylineOnSphere(tessellated_geometry)


    MOR = pygplates.Feature.create_tectonic_section(
        pygplates.FeatureType.gpml_mid_ocean_ridge,
        tessellated_geometry,
        # to prevent problems with two instances of the same MOR add 0.01 Myr
        valid_time=(reconstruction_time, reconstruction_time - timestep + 0.01),
        left_plate = plate_ids[0],
        right_plate = plate_ids[1],
        reconstruction_method = 'HalfStageRotationVersion3',
        )
    
    MOR_out.add(MOR)
    
    return MOR_out

def get_anti_pole(pole):
    # this function provides pole2, which is the antipodal point of pole1
    # it doesn't always work to calculate like pole1, but reversed moving plate and anchor plate
    pole2 = [-coord for coord in pole]
    pole2 = pygplates.PointOnSphere(pole2) 
    return pole2

def reconstruct_pole(pole, rotation_model, reconstruction_time, plate_ids):
    # this function reconstructs the pole to it's position at a time
    # this is required as in subsequent times the pole is rotated around other poles
    # volcano seems the only reconstructable point feature
    pole_feature = pygplates.Feature.create_reconstructable_feature(
    pygplates.FeatureType.gpml_unclassified_feature,
    pole,
    valid_time = (reconstruction_time, 0),
    reconstruction_plate_id = plate_ids[1],
    )

    pole_recon = []
    pygplates.reconstruct(pole_feature, rotation_model, pole_recon, reconstruction_time)
    pole = pole_recon[0].get_reconstructed_geometry()
    
    return pole

def get_stage_poles(plate_ids, begin_time, rotation_model, timestep):
    # this function determines the stage poles for two plates
    pole1 = get_stage_pole(plate_ids, begin_time, rotation_model, timestep)
    pole2 = get_anti_pole(pole1)
    pole1 = pygplates.PointOnSphere(pole1)
    pole2 = pygplates.PointOnSphere(pole2)
    pole1 = reconstruct_pole(pole1, rotation_model, begin_time, plate_ids)
    pole2 = reconstruct_pole(pole2, rotation_model, begin_time, plate_ids)
    return pole1, pole2

def create_isochron_features(MOR_geometry, plate_ids, rotation_model, time):
    # this function takes a geometry and converts it to GPlates isochron features 
    # for both sides of the mid ocean ridge
    left_isochron_feature = pygplates.Feature.create_reconstructable_feature(
        pygplates.FeatureType.gpml_isochron,
        MOR_geometry,
        valid_time = (time, 0),
        reconstruction_plate_id = plate_ids[0],
        conjugate_plate_id = plate_ids[1],
        reverse_reconstruct = (rotation_model, time)
        )

    right_isochron_feature = pygplates.Feature.create_reconstructable_feature(
        pygplates.FeatureType.gpml_isochron,
        MOR_geometry,
        valid_time = (time, 0),
        reconstruction_plate_id = plate_ids[1],
        conjugate_plate_id = plate_ids[0],
        reverse_reconstruct = (rotation_model, time)
        )
    return left_isochron_feature, right_isochron_feature

def create_stepped_lines(plate_ids, rotation_model, begin_time, end_time, timestep, curve, MOR_out, on_intermediate, tessellate, tessellate_radians):
    # this function runs other functions to convert a curve to a mid oceanic ridge geometry
    MORs1 = pygplates.FeatureCollection()
    MORs2 = pygplates.FeatureCollection()
    tessellated_curves = pygplates.FeatureCollection()
    transforms = pygplates.FeatureCollection()

    reconstructed_curves = get_curve_geometry(curve, rotation_model, begin_time, plate_ids)

    pole1, pole2 = get_stage_poles(plate_ids, begin_time, rotation_model, timestep)

    for curve in reconstructed_curves:
        curve = curve.to_xyz_array()
        
        # tessellated_curve contains more points with maximum distance between points
        if tessellate:
            tessellated_curve = tessellate_curve(curve, plate_ids, tessellate_radians, tessellated_curves, begin_time, end_time, rotation_model)
            curve = tessellated_curve.to_xyz_array()

        create_segments(curve, pole1, pole2, begin_time, end_time, plate_ids, MORs1, MORs2, transforms, rotation_model, on_intermediate)

        # get the points where the great circles intersect with their respective small circles
        intersections = get_intersections(MORs1, MORs2, transforms, rotation_model, begin_time, on_intermediate, curve)
        # depending on the direction of drawing, it can be necessary to put the MORs in another order
        if len(intersections) <= 2:
            intersections = get_intersections(MORs2, MORs1, transforms, rotation_model, begin_time, on_intermediate, curve)
        if len(intersections) >= 2:
            MOR_out = intersections_to_MOR(intersections, begin_time, end_time, timestep, plate_ids, rotation_model, MOR_out)

    return MOR_out

def make_isochron(times, plate_ids, rotation_model, timestep, MOR_out, isochron_feature_collection):
    # this function runs other functions to make a feature collection consisting of isochrons
    begin_time = times[0]
    for time in times:
        pole_at_time = get_stage_pole(plate_ids, time, rotation_model, timestep)
        pole1, pole2 = get_stage_poles(plate_ids, begin_time, rotation_model, timestep)

        if pole_at_time != pole1:
            pole1 = pole_at_time          

        reconstructed_ridges = []
        pygplates.reconstruct(MOR_out, rotation_model, reconstructed_ridges, time)

        MOR_geometry = [MOR.get_reconstructed_geometry() for MOR in reconstructed_ridges]

        left_isochron_feature, right_isochron_feature = create_isochron_features(MOR_geometry, plate_ids, rotation_model, time)

        # Add isochrons to feature collection.
        isochron_feature_collection.add(left_isochron_feature)
        isochron_feature_collection.add(right_isochron_feature)
    
    return isochron_feature_collection

def get_pole_changes(plate_ids, begin_time, end_time, timestep, smooth_step, rotation_model):
    # this function calculates at which times a change in pole occurs
    # these times are added to a list of times at which isochrons are calculated
    change_times = [begin_time]
    times = np.arange(begin_time, end_time, - timestep)
    
    # get the pole at the start of the reconstruction
    pole0 = get_stage_pole(plate_ids, begin_time, rotation_model, timestep)
    pole0 = pygplates.PointOnSphere(pole0)
    pole0 = reconstruct_pole(pole0, rotation_model, begin_time, plate_ids)

    for time in times:
        # determine the pole at consecutive time steps
        pole1 = get_stage_pole(plate_ids, time, rotation_model, timestep)
        pole1 = pygplates.PointOnSphere(pole1)
        pole1 = reconstruct_pole(pole1, rotation_model, begin_time, plate_ids)
        
        # check if the pole has changed since the previous time
        if pole1 != pole0:
            # add a series of timesteps at higher resolution to smoothen the change
            for i in range(timestep, -1, -smooth_step):
                change_times.append(time+i)
            pole0 = pole1

    change_times.append(end_time)

    return change_times

def split_feature_collection(feature_collection):
    # this function takes a feature collection and splits it in features
    # which are or are not in topological features
    # this function is not used and I don't remember what to use it for
    topologies = []
    lines_in_topos = []
    lines_not_in_topos = []

    for feature in feature_collection:
        if feature.get_feature_type().get_name() == 'MidOceanRidge':
            for p in feature:
                if "GpmlTopologicalSection" in str(p.get_value()) and p.get_name() == pygplates.PropertyName.gpml_center_line_of:
                    topological_line_feature = feature
                    topologies.append(topological_line_feature)

                    for p_topo in topological_line_feature:
                        if p_topo.get_name() == pygplates.PropertyName.gml_name:
                            plate_ids_list = str(p_topo.get_value()).split(';')
                            plate_ids_list = [string.split(',') for string in plate_ids_list]
                            plate_ids_list = [list(map(int, row)) for row in plate_ids_list]

                    for plate_ids in plate_ids_list:
                        line_features = select_curves(feature_collection, plate_ids)
                        for feature in line_features:
                            lines_in_topos.append(feature)
            
    for feature in feature_collection:
        if feature not in topologies and feature not in lines_in_topos:
            lines_not_in_topos.append(feature)

    topologies = pygplates.FeatureCollection(topologies)
    lines_in_topos = pygplates.FeatureCollection(lines_in_topos)
    lines_not_in_topos = pygplates.FeatureCollection(lines_not_in_topos)

    return topologies, lines_in_topos, lines_not_in_topos

def create_segments(curve, pole1, pole2, begin_time, end_time, plate_ids, MORs1, MORs2, transforms, rotation_model, on_intermediate):
    # get intermediate points between the points that define the curve
    # these points with the stage poles define the great circles 
    intermediate_points = get_intermediate_points(curve)
    #print('number of intermediate points ' + str(len(intermediate_points)))

    if on_intermediate == 'ridges':
        # create great circles with the poles and the points
        for point in intermediate_points:
            MORs1 = create_greatcircles(point, pole1, begin_time, plate_ids, MORs1, rotation_model)
            MORs2 = create_greatcircles(point, pole2, begin_time, plate_ids, MORs2, rotation_model)
        #print('number of great circles ' + str(len(MORs1)))

        for point in curve:
            transform_vec = create_transforms(pole1, point)
            transforms = points_to_transform(point, transform_vec, begin_time, end_time, rotation_model, transforms, plate_ids, on_intermediate)
        #print('number of transforms ' + str(len(transforms)))

    if on_intermediate == 'transforms':
        # create great circles with the poles and the points
        for point in curve:
            MORs1 = create_greatcircles(point, pole1, begin_time, plate_ids, MORs1, rotation_model)
            MORs2 = create_greatcircles(point, pole2, begin_time, plate_ids, MORs2, rotation_model)
        #print('number of great circles ' + str(len(MORs1)))

        for point in intermediate_points:
            transform_vec = create_transforms(pole1, point)
            transforms = points_to_transform(point, transform_vec, begin_time, end_time, rotation_model, transforms, plate_ids, on_intermediate)
        #print('number of transforms ' + str(len(transforms)))

    return MORs1, MORs2, transforms

def create_isochrons_for_boundary_topologies(feature_collection, rotation_model, from_time, to_time, timestep, isochron_feature_collection, tessellate, tessellate_radians, model, on_intermediate):
    # this function creates isochrons from mid ocean ridges that form part of boundary topologies

    reconstruction_times = list(range(from_time, to_time-timestep, -timestep))
    for reconstruction_time in reconstruction_times:
        end_time = reconstruction_time - timestep
        print(f'starting time {reconstruction_time}')

        resolved_topologies = []
        pygplates.resolve_topologies(
            feature_collection,
            rotation_model,
            resolved_topologies,
            reconstruction_time)

        for resolved_topology in resolved_topologies:

            line_sub_segments = resolved_topology.get_boundary_sub_segments()

            clipped_features = [line_sub_segment.get_resolved_feature()
                for line_sub_segment in line_sub_segments]

            for feature in clipped_features:
                MORs1 = pygplates.FeatureCollection()
                MORs2 = pygplates.FeatureCollection()
                tessellated_curves = pygplates.FeatureCollection()
                transforms = pygplates.FeatureCollection()
                intersections = []
                MOR_out = pygplates.FeatureCollection()

                if feature.get_feature_type().get_name() == 'MidOceanRidge':
                    plate_ids = [feature.get_left_plate(), feature.get_right_plate()]
                    pole1, pole2 = get_stage_poles(plate_ids, reconstruction_time, rotation_model, timestep)

                    curves = feature.get_geometries()
                    
                    for curve in curves:
                        curve = curve.to_xyz_array()
                                    
                        # tessellated_curve contains more points with maximum distance between points
                        if tessellate:
                            tessellated_curve = tessellate_curve(curve, plate_ids, tessellate_radians, tessellated_curves, reconstruction_time, end_time, rotation_model)
                            curve = tessellated_curve.to_xyz_array()
                        
                        MORs1, MORs2, transforms = create_segments(curve, pole1, pole2, reconstruction_time, end_time, plate_ids, MORs1, MORs2, transforms, rotation_model, on_intermediate)

                        # get the points where the great circles intersect with their respective small circles
                        intersections = get_intersections(MORs1, MORs2, transforms, rotation_model, reconstruction_time, on_intermediate, curve)
                        # depending on the direction of drawing, it can be necessary to put the MORs in another order
                        if len(intersections) <= 2:
                            intersections = get_intersections(MORs2, MORs1, transforms, rotation_model, reconstruction_time, on_intermediate, curve)
                        if len(intersections) >= 2:
                            MOR_out = intersections_to_MOR(intersections, reconstruction_time, end_time, timestep, plate_ids, rotation_model, MOR_out)

                            reconstructed_ridges = []
                            pygplates.reconstruct(MOR_out, rotation_model, reconstructed_ridges, reconstruction_time)

                            MOR_geometry = [MOR.get_reconstructed_geometry() for MOR in reconstructed_ridges]

                            left_isochron_feature, right_isochron_feature = create_isochron_features(MOR_geometry, plate_ids, rotation_model, reconstruction_time)

                            # Add isochrons to feature collection.
                            isochron_feature_collection.add(left_isochron_feature)
                            isochron_feature_collection.add(right_isochron_feature)

    return isochron_feature_collection


def check_for_subduction_individual_isochrons(isochron_feature_collection, feature_collection, rotation_model, times, timestep, isochron_features_not_subducted, subducted_isochrons, other_isochrons):
    # this function takes a feature collection with isochrons 
    # and checks which parts of the feature are in a plate with the same plate id
    # if not they are considered subducted (could also be thrusted) 
    # and removed from the feature collection
    subduction_zone_features = pygplates.FeatureCollection()
    mor_features = pygplates.FeatureCollection()

    for feature in feature_collection:
        if feature.get_feature_type().get_name() == 'SubductionZone':
            subduction_zone_features.add(feature)
        if feature.get_feature_type().get_name() == 'MidOceanRidge':
            mor_features.add(feature)

    for isochron_feature in isochron_feature_collection:
        begin_time, end_time = isochron_feature.get_valid_time()

        for time in times:

            # if the isochron is created at the time it cannot have subducted yet
            # as the plates can be curves and the isochrons stepped they might be thrown out
            # therefore keep every isochron at its creation time

            if begin_time == time:
                reconstruction_plate_id = isochron_feature.get_reconstruction_plate_id()
                conjugate_plate_id = isochron_feature.get_conjugate_plate_id()
                new_isochron_feature = pygplates.Feature.create_reconstructable_feature(
                    pygplates.FeatureType.gpml_isochron,
                    isochron_feature.get_geometry(),
                    valid_time = (time, time),
                    reconstruction_plate_id = reconstruction_plate_id,
                    conjugate_plate_id = conjugate_plate_id,
                    )

                new_isochron_feature.set_shapefile_attribute('isochron_age', begin_time - time)
                    
                isochron_features_not_subducted.add(new_isochron_feature)

            else:
                reconstructed_geometries = []
                pygplates.reconstruct(isochron_feature, rotation_model, reconstructed_geometries, time)
                for reconstructed_geometry in reconstructed_geometries:
                    geometry = reconstructed_geometry.get_reconstructed_geometry()
                    feature = reconstructed_geometry.get_feature()
                    reconstruction_plate_id = feature.get_reconstruction_plate_id()
                    conjugate_plate_id = feature.get_conjugate_plate_id()
                    plate_id = feature.get_reconstruction_plate_id()

                    rec_isochron_feature = pygplates.Feature.create_reconstructable_feature(
                        pygplates.FeatureType.gpml_isochron,
                        geometry,
                        valid_time = (time, time),
                        reconstruction_plate_id = reconstruction_plate_id,
                        conjugate_plate_id = conjugate_plate_id,
                        )

                    # unpartitioned features are outside all plates and so will not be used
                    partitioned_features, unpartitioned_features = pygplates.partition_into_plates(
                        feature_collection,
                        rotation_model,
                        rec_isochron_feature,
                        reconstruction_time=time,
                        partition_method=pygplates.PartitionMethod.split_into_plates,
                        partition_return=pygplates.PartitionReturn.separate_partitioned_and_unpartitioned)

                    for partitioned_feature in partitioned_features:

                        partitioned_geometries = partitioned_feature.get_geometries()
                        reconstruction_plate_id = partitioned_feature.get_reconstruction_plate_id()
                        conjugate_plate_id = partitioned_feature.get_conjugate_plate_id()
                           
                        for partitioned_geometry in partitioned_geometries:
                            
                            assigned_feature = pygplates.Feature.create_reconstructable_feature(
                                pygplates.FeatureType.gpml_isochron,
                                partitioned_geometry,
                                valid_time = (time, time),
                                reconstruction_plate_id = reconstruction_plate_id,
                                conjugate_plate_id = conjugate_plate_id,
                                )

                            assigned_feature.set_shapefile_attribute('isochron_age', begin_time - time)

                            temp_geometries = []
                            pygplates.reconstruct(assigned_feature, rotation_model, temp_geometries, time)

                            for temp_geometry in temp_geometries:
                                assigned_feature = temp_geometry.get_feature()

                                # if assigned plate id equals the original it still exists and continues on the same plate
                                if assigned_feature.get_reconstruction_plate_id() == plate_id:
                                    isochron_features_not_subducted.add(assigned_feature)
                                    
                                    # the subducted part has to be removed for the next timestep
                                    assigned_geometry = assigned_feature.get_geometry()
                                    isochron_feature = pygplates.Feature.create_reconstructable_feature(
                                        pygplates.FeatureType.gpml_isochron,
                                        assigned_geometry,
                                        valid_time = (time-timestep, time-timestep),
                                        reconstruction_plate_id = reconstruction_plate_id,
                                        conjugate_plate_id = conjugate_plate_id,
                                    )

                                else:
                                    distances = []
                                    # if it touches a subduction zone, it is probably subducted
                                    for subduction_zone in subduction_zone_features:
                                        isochron_geometry = assigned_feature.get_geometry()
                                        subduction_zone_geometries = subduction_zone.get_geometries()
                                        for subduction_zone_geometry in subduction_zone_geometries:
                                            distance = pygplates.GeometryOnSphere.distance(isochron_geometry, subduction_zone_geometry)
                                            distances.append(distance)
                                    if 0 in distances:
                                        subducted_isochrons.add(assigned_feature)

                                    # if it touches a MOR it should not be modified
                                    for mor in mor_features:
                                        isochron_geometry = assigned_feature.get_geometry()
                                        mor_geometries = mor.get_geometries()
                                        for mor_geometry in mor_geometries:
                                            distance = pygplates.GeometryOnSphere.distance(isochron_geometry, mor_geometry)
                                            distances.append(distance)
                                    if 0 in distances:
                                        isochron_features_not_subducted.add(assigned_feature)

                                    # if a new plate has formed 
                                    else:
                                        other_isochrons.add(assigned_feature)
                                        # the modified isochron has to be put back into the collection for the next timestep
                                        add_back_other_isochron = True
                                        if add_back_other_isochron:
                                            assigned_geometry = assigned_feature.get_geometry()
                                            isochron_feature = pygplates.Feature.create_reconstructable_feature(
                                                pygplates.FeatureType.gpml_isochron,
                                                assigned_geometry,
                                                valid_time = (time-timestep, time-timestep),
                                                reconstruction_plate_id = reconstruction_plate_id,
                                                conjugate_plate_id = conjugate_plate_id,
                                            )


    return isochron_features_not_subducted, subducted_isochrons, other_isochrons

def create_age_grid(isochron_feature_collection, rotation_model, time, age_in, res, model, region):
    # this function takes a feature collection with isochrons and converts this to a grid with ages
    reconstructed_isochrons = []
    latlonages = []
    pygplates.reconstruct(isochron_feature_collection, rotation_model, reconstructed_isochrons, time)

    for geometry in reconstructed_isochrons:
        feature = geometry.get_feature()
        if age_in == 'isochron_age':
            age = feature.get_shapefile_attribute('isochron_age') - time

        else:
            age = feature.get_valid_time()[0] - time

        for point in geometry.get_reconstructed_geometry().to_lat_lon_list():
            latlonages.append([point[1], point[0], age])

    np.savetxt(f'../data/temp/latlonages_{time}.gmt', latlonages, delimiter=' ')

    input_file = f"../data/temp/latlonages_{time}.gmt"
    blockmean_grid = "../data/temp/blockmean_grid.gmt"
    
    # determine the mean per block
    os.system(f"gmt blockmean {input_file} -R{region} -I{res} > {blockmean_grid} -V")
  
    # GMT has several options to interpolate. This way you can compare the results
    methods = ['sphinterpolate']
    for method in methods:
        interpolated_grid = f"../data/temp/age-grid_{model}_{time}Ma.nc"
        if method == 'sphinterpolate':
            os.system(f"gmt sphinterpolate {blockmean_grid} -R{region} -I{res} -G{interpolated_grid} -V")
        if method == 'surface':
            os.system(f"gmt surface {blockmean_grid} -R{region} -I{res} -G{interpolated_grid} -V")

def make_grid_for_mask(res, continents_features, rotation_model):
    # this function makes a grid, with assigned plate ids
    # this grid is later used to make a mask for the interpolation
    
    x_range= 360
    y_range= 180
    
    x_min, x_max = - x_range / 2, x_range / 2
    y_min, y_max = - y_range / 2, y_range / 2
    
    nx, ny = (int(((x_range)/res)), int((y_range/res)))
    x = np.linspace(x_min+res, x_max-res, nx-1)
    y = np.linspace(y_max-res, y_min+res, ny-1)
    
    Mask=np.zeros((ny, nx))
    Lon,Lat=np.meshgrid(x, y)

    points = np.dstack((Lon,Lat))
    points = points.reshape(((ny-1)*(nx-1),2))

    np.savetxt('../data/temp/grid.gmt', points, delimiter='\t')

    # Load a GMT file (instead of manually reading a '.txt' file line-by-line).
    point_features = pygplates.FeatureCollection('../data/temp/grid.gmt')

    # Use the static polygons to assign plate IDs and valid time periods.
    # Each point feature is partitioned into one of the static polygons and assigned its
    # reconstruction plate ID and valid time period.
    assigned_point_features = pygplates.partition_into_plates(
        continents_features,
        rotation_model,
        point_features,
        properties_to_copy = [
            pygplates.PartitionProperty.reconstruction_plate_id],
        partition_return = pygplates.PartitionReturn.separate_partitioned_and_unpartitioned)

    assigned_point_features = assigned_point_features[0]

    # Write the assigned point features to a GMT file
    assigned_point_feature_collection = pygplates.FeatureCollection(assigned_point_features)
    assigned_point_feature_collection.write('../data/temp/grid_plate_ids_assigned.gmt')
    return (assigned_point_feature_collection)

def make_boundary_topology(feature_collection, plate_id, time, timestep):
    # this function takes a feature collection which should have only features
    # for this plate and builds a boundary topology feature from that
    sections = []
    for feature in feature_collection:
        if feature is not None:
            sections.append(pygplates.GpmlTopologicalSection.create(
                feature,
                topological_geometry_type=pygplates.GpmlTopologicalLine))

    if len(sections) > 1:
        topological_polygon_feature = pygplates.Feature.create_topological_feature(
            pygplates.FeatureType.gpml_topological_closed_plate_boundary,
            pygplates.GpmlTopologicalPolygon(sections),
            # to prevent problems with multiple instances of the same boundary topology, add 0.01 Myr
            valid_time = (time, time - timestep + 0.01))

        topological_polygon_feature.set_reconstruction_plate_id(plate_id)

        return topological_polygon_feature
    
    else:
        pass

def make_plate_id_list(feature_collection):
    # this function makes a list of plate ids in a feature collection
    plate_id_list = []
    for feature in feature_collection:
        if feature.get_reconstruction_method() == 'ByPlateId':
            plate_id_list.append(feature.get_reconstruction_plate_id())
            plate_id_list.append(feature.get_conjugate_plate_id())
        else:
            plate_id_list.append(feature.get_left_plate())
            plate_id_list.append(feature.get_right_plate())
    plate_id_list = list(set(plate_id_list))
    return plate_id_list


def get_plate_features(feature_collection, plate_id):
    # this function selects features with a given plate id from a feature collection

    feature_types = ['MidOceanRidge', 'SubductionZone', 'Transform', "ContinentalRift"]
    
    plate_features = []
    for feature in feature_collection:
        if feature.get_feature_type().get_name() in feature_types:
            # for non MOR features, either the reconstruction or the conjugate plate id must be the one of the polygon being made
            if feature.get_right_plate() == plate_id or feature.get_left_plate() == plate_id:
                plate_features.append(feature)

            elif feature.get_reconstruction_plate_id() == plate_id:
                plate_features.append(feature)
            
            else:
                for conjugate_plate_id in feature.get_conjugate_plate_id([], pygplates.PropertyReturn.all):
                    if conjugate_plate_id == plate_id:
                        plate_features.append(feature)

    return plate_features

def get_ordered_features(reconstructed_feature_geometries):
    # this features takes a collection of features
    # and orders them by checking whether they cut or are close
    # this is then used to make boundary topologies
    ordered_features = []

    reconstructed_feature_geometry0 = reconstructed_feature_geometries[0] # only for check_cutting
    reconstructed_feature_geometry1 = reconstructed_feature_geometries.pop(0)
    feature1 = reconstructed_feature_geometry1.get_feature()
    geometry1 = reconstructed_feature_geometry1.get_reconstructed_geometry()
    ordered_features.append(feature1)

    # check_cutting gets all features if they cut, but not if they are close
    check_cutting = True
    if check_cutting:
        max_distance = 0.
        checked_nr = 0

        while len(reconstructed_feature_geometries) > 0 and len(reconstructed_feature_geometries) != checked_nr - 1:
            reconstructed_feature_geometry2 = reconstructed_feature_geometries.pop(0)
            geometry2 = reconstructed_feature_geometry2.get_reconstructed_geometry()
            distance = pygplates.GeometryOnSphere.distance(geometry1, geometry2)
            if distance <= max_distance:
                reconstructed_feature_geometry1 = reconstructed_feature_geometry2
                feature1 = reconstructed_feature_geometry1.get_feature()
                geometry1 = reconstructed_feature_geometry1.get_reconstructed_geometry()
                ordered_features.append(feature1)
                checked_nr = 0
            # TODO: should this test increasing distances? It should cut otherwise you get strange connections
            else:
                reconstructed_feature_geometries.append(reconstructed_feature_geometry2)
                checked_nr += 1

        # check the unordered features again starting from the first of the ordered
        # then insert at the front
        checked_nr = 0
        feature1 = reconstructed_feature_geometry0.get_feature()
        geometry1 = reconstructed_feature_geometry0.get_reconstructed_geometry()

        while len(reconstructed_feature_geometries) > 0 and len(reconstructed_feature_geometries) != checked_nr - 1:
            reconstructed_feature_geometry2 = reconstructed_feature_geometries.pop(0)
            geometry2 = reconstructed_feature_geometry2.get_reconstructed_geometry()
            distance = pygplates.GeometryOnSphere.distance(geometry1, geometry2)
            if distance <= max_distance:
                reconstructed_feature_geometry1 = reconstructed_feature_geometry2
                feature1 = reconstructed_feature_geometry1.get_feature()
                geometry1 = reconstructed_feature_geometry1.get_reconstructed_geometry()
                ordered_features.insert(0, feature1)
                checked_nr = 0
            else:
                reconstructed_feature_geometries.append(reconstructed_feature_geometry2)
                checked_nr += 1

        # add the features that were not ordered.
        add_unordered_features = False
        if add_unordered_features:
            for geometry in reconstructed_feature_geometries:
                ordered_features.append(geometry.get_feature())

    check_closest = False
    if check_closest:
        while len(reconstructed_feature_geometries) > 0:
            distances = []
            for reconstructed_feature_geometry in reconstructed_feature_geometries:
                geometry2 = reconstructed_feature_geometry.get_reconstructed_geometry()
                distances.append(pygplates.GeometryOnSphere.distance(geometry1, geometry2))

            min_distance_index = np.argmin(distances)

            closest_geometry = reconstructed_feature_geometries.pop(min_distance_index)
            feature1 = closest_geometry.get_feature()
            geometry1 = closest_geometry.get_reconstructed_geometry()

            ordered_features.append(feature1)

    ordered_features = pygplates.FeatureCollection(ordered_features)

    return ordered_features