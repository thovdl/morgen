# README #

### What is this repository for? ###

This repository contains the code of MORGen: an algorithm to compute from simple input realistic mid-ocean ridges in plate models.

### How do I get set up? ###

The code requires:

* Python 3.7
* pyGPlates revision 28 - [download from sourceforge](https://sourceforge.net/projects/gplates/files/pygplates/beta-revision-28/)
With minor modifications the code can also run in other versions of Python and pyGPlates

Python libraries to be installed:

* NumPy
* Pandas
* NetCDF4, only if you want to create age or bathymetry grids

Other:

* GMT, only if you want to create age or bathymetry grids

Required folder structure:
```
.
├── .gitignore
├── CITATION.md
├── LICENSE.md
├── README.md
├── bin                
│   └── external                <- Contains pyGPlates for Windows and Mac
├── data                        <- All project data, ignored by git
│   ├── raw                     <- Contains 
│   │   └─ plateReconstructions <- Contains plate reconstructions in separate folders
│   └── temp                    <- Intermediate data that has been transformed
└── src                         <- Source code for this project

```
Note:
pyGPlates in Linux (Ubuntu) is standard installed in /usr/lib/pygplates/revision28/, the code is set up to use this directory.

### Running the code ####

There are three separate files with the code:

* MORGen_set_parameters -> sets the input parameters and options, this should be the only file to modify
* MORGen_run_code -> takes the input parameters and runs the required functions
* MORGen_functions -> contains the functions

Output is stored in the data/temp directory. Depending on the chosen options different file names are produced with descriptive file names.

### Test / reproduce results from the article ###

* MORGen_set_parameters is setup to run with sample input for the _southern oceans_ model used in the article. To run with the other sample models change the _model_ variable to one of:
	- future
	- mediterranean


### Who do I talk to? ###

* Thomas van der Linden