### Citation ###
If you use this code, please reference the article:
van der Linden, T.J.M., and van Hinsbergen, D.J.J., submitted, MORGen: an algorithm to compute from simple input realistic mid-ocean ridges in plate models, Solid Earth